package org.bulbagarden.test;

import android.support.annotation.StringRes;
import android.support.test.runner.AndroidJUnit4;

import org.junit.runner.RunWith;
import org.bulbagarden.Site;
import org.bulbagarden.WikipediaApp;
import org.bulbagarden.editing.EditTokenStorage;
import org.bulbagarden.testlib.TestLatch;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static android.support.test.InstrumentationRegistry.getInstrumentation;

@RunWith(AndroidJUnit4.class)
public class LoginTaskTest {
    private static final Site TEST_WIKI_SITE = new Site("test.wikipedia.org");
    private static final String SUCCESS = "Success";

    private final WikipediaApp app = WikipediaApp.getInstance();
    private final TestLatch completionLatch = new TestLatch();





    private EditTokenStorage.TokenRetrievedCallback callback = new EditTokenStorage.TokenRetrievedCallback() {
        @Override
        public void onTokenRetrieved(String token) {
            assertThat(token.equals("+\\"), is(false));
            completionLatch.countDown();
        }

        @Override
        public void onTokenFailed(Throwable caught) {
            throw new RuntimeException(caught);
        }
    };

    private void runOnMainSync(Runnable r) {
        getInstrumentation().runOnMainSync(r);
    }

    private static String getString(@StringRes int id) {
        return getInstrumentation().getContext().getString(id);
    }
}