package org.bulbagarden.server.restbase;

import org.bulbagarden.page.Page;
import org.bulbagarden.page.Section;
import org.bulbagarden.server.PageRemaining;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * Gson POJO for loading remaining page content.
 */
public class RbPageRemaining implements PageRemaining {
    @Nullable private List<Section> sections;

    @Nullable
    public List<Section> getSections() {
        return sections;
    }

    @Override
    public void mergeInto(Page page) {
        page.augmentRemainingSections(getSections());
    }
}
