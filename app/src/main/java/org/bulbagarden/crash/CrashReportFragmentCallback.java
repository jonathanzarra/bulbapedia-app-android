package org.bulbagarden.crash;

import org.bulbagarden.activity.FragmentCallback;

public interface CrashReportFragmentCallback extends FragmentCallback {
    void onStartOver();
    void onQuit();
}