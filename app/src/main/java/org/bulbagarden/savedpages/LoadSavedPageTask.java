package org.bulbagarden.savedpages;

import org.bulbagarden.page.PageTitle;
import org.bulbagarden.concurrency.SaneAsyncTask;
import org.bulbagarden.page.Page;

public class LoadSavedPageTask extends SaneAsyncTask<Page> {
    private final PageTitle title;
    private final int sequence;

    public LoadSavedPageTask(PageTitle title) {
        this(title, 0);
    }

    public LoadSavedPageTask(PageTitle title, int sequence) {
        this.title = title;
        this.sequence = sequence;
    }

    @Override
    public Page performTask() throws Throwable {
        SavedPage savedPage = new SavedPage(title);
        return savedPage.readFromFileSystem();
    }

    public int getSequence() {
        return sequence;
    }
}
