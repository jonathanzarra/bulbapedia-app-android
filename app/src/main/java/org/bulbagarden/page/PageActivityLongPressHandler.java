package org.bulbagarden.page;

import android.support.annotation.NonNull;

import org.bulbagarden.R;
import org.bulbagarden.history.HistoryEntry;
import org.bulbagarden.readinglist.AddToReadingListDialog;
import org.bulbagarden.util.ClipboardUtil;
import org.bulbagarden.util.FeedbackUtil;
import org.bulbagarden.util.ShareUtil;

public abstract class PageActivityLongPressHandler implements PageLongPressHandler.ContextMenuListener {
    @NonNull
    private final PageActivity activity;

    public PageActivityLongPressHandler(@NonNull PageActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onOpenLink(PageTitle title, HistoryEntry entry) {
        activity.loadPage(title, entry);
    }

    @Override
    public void onOpenInNewTab(PageTitle title, HistoryEntry entry) {
        activity.loadPage(title, entry, PageActivity.TabPosition.NEW_TAB_BACKGROUND, false);
    }

    @Override
    public void onCopyLink(PageTitle title) {
        copyLink(title.getCanonicalUri());
        showCopySuccessMessage();
    }

    @Override
    public void onShareLink(PageTitle title) {
        ShareUtil.shareText(activity, title);
    }

    @Override
    public void onAddToList(PageTitle title, AddToReadingListDialog.InvokeSource source) {
        activity.showAddToListDialog(title, source);
    }

    private void copyLink(String url) {
        ClipboardUtil.setPlainText(activity, null, url);
    }

    private void showCopySuccessMessage() {
        FeedbackUtil.showMessage(activity, R.string.address_copied);
    }
}
