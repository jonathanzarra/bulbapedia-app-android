package org.bulbagarden.useroption.dataclient;

import org.bulbagarden.Site;

public final class UserOptionDataClientSingleton {
    public static UserOptionDataClient instance() {
        return LazyHolder.INSTANCE;
    }

    private UserOptionDataClientSingleton() { }

    private static class LazyHolder {
        private static final UserOptionDataClient INSTANCE = instance();

        private static UserOptionDataClient instance() {
            Site site = new Site("meta.wikimedia.org", "");
            return new DefaultUserOptionDataClient(site);
        }
    }
}