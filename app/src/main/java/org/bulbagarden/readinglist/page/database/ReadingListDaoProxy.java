package org.bulbagarden.readinglist.page.database;

import android.support.annotation.NonNull;
import android.util.Base64;

import org.bulbagarden.page.Namespace;
import org.bulbagarden.page.PageTitle;
import org.bulbagarden.readinglist.ReadingList;
import org.bulbagarden.readinglist.page.ReadingListPage;
import org.bulbagarden.readinglist.page.database.disk.DiskStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ReadingListDaoProxy {

    public static List<PageTitle> pageTitles(@NonNull Collection<ReadingListPage> pages) {
        List<PageTitle> titles = new ArrayList<>();
        for (ReadingListPage page : pages) {
            titles.add(pageTitle(page));
        }
        return titles;
    }

    @NonNull
    public static PageTitle pageTitle(@NonNull ReadingListPage page) {
        return new PageTitle(page.title(), page.site(), page.thumbnailUrl(), page.description());
    }

    @NonNull
    public static ReadingListPage page(@NonNull ReadingList list, @NonNull PageTitle title) {
        long now = System.currentTimeMillis();
        Namespace namespace;
        try {
            namespace = title.getNamespace() == null ? Namespace.MAIN : Namespace.of(Integer.parseInt(title.getNamespace()));
        } catch (NumberFormatException e) {
            namespace = Namespace.MAIN;
        }
        return ReadingListPage
                .builder()
                .diskStatus(list.getSaveOffline() ? DiskStatus.OUTDATED : DiskStatus.ONLINE)
                .key(key(title))
                .listKeys(listKey(list))
                .site(title.getSite())
                // TODO: unmarshal namespace when received, not when used.
                .namespace(namespace)
                .title(title.getDisplayText())
                .diskPageRevision(title.hasProperties() ? title.getProperties().getRevisionId() : 0)
                .mtime(now)
                .atime(now)
                .thumbnailUrl(title.hasProperties() ? title.getProperties().getLeadImageUrl() : null)
                .description(title.getDescription())
                .build();
    }

    @NonNull
    public static String key(@NonNull PageTitle title) {
        // TODO: this should use the following but PageTitles often do not have Properties and page
        //       ID is not preserved elsewhere.
        // return "wikipedia-" + title.getSite().languageCode() + '-' + title.getProperties().getPageId();
        return Base64.encodeToString((title.getSite().languageCode() + '-' + title.getDisplayText()).getBytes(),
                Base64.NO_WRAP);
    }

    @NonNull
    public static String listKey(@NonNull ReadingList list) {
        // TODO: we need to rekey all pages if a user changes the list title.
        return listKey(list.getTitle());
    }

    @NonNull
    public static String listKey(@NonNull String title) {
        // TODO: we need to rekey all pages if a user changes the list title.
        return Base64.encodeToString(title.getBytes(), Base64.NO_WRAP);
    }

    private ReadingListDaoProxy() {
    }
}